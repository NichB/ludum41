﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderController : MonoBehaviour {

    private Text textObject;

    private void Start() {
        textObject = GetComponent<Text>();
    }

    public void ChangePoints(int newPoints) {
        textObject.text = "Points: " + newPoints.ToString();
    }
}
