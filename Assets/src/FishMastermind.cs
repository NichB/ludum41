﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMastermind : MonoBehaviour {

    public int noFish = 15;

    public List<GameObject> FishPrefabs;

    public bool battleRoyale = false;

    public float tickrate = 1f;

    private float tickTimeout = 0f;
    private List<GameObject> Fishes = new List<GameObject>();

	// Use this for initialization
	void Start () {
        //Instantiate fishes
        Vector2 newPos = Vector2.zero;
        for (int i = 0; i < noFish; i++)
        {
            newPos = new Vector2(Random.Range(-33f, 33f), Random.Range(-18f, 18f));
            InstantiateFish( newPos );
        }


    }

    private void FixedUpdate() {
        if (battleRoyale && tickTimeout < Time.time)
        {
            //print("In here");
            // print(Fishes.Count.ToString());
            tickTimeout = Time.time + tickrate;
            List<GameObject> fishFound;
            GameObject chosenFish;
            float detectionRange = 10f;

            FishBrain fishy;
            foreach (GameObject fish in Fishes)
            {
                if (fish == null)
                {
                    //break;
                }
                fishy = fish.GetComponent<FishBrain>();
                if (!fishy.attacking && !fishy.dead)
                {
                    fishFound = FishCloserThan(new Vector2(fish.transform.position.x, fish.transform.position.y), detectionRange);
                    chosenFish = fishFound[Random.Range(0, fishFound.Count - 1)];
                    if (chosenFish != fish)
                    {
                        fishy.Attack(chosenFish);
                    }
                }
                if (!fishy.battleRoyale)
                {
                    fishy.battleRoyale = true;
                }
            }
        }
    }
	
    public void Battle() {
        battleRoyale = true;
    }

    public int getFishCount() {
        return (Fishes.Count);
    }

    public GameObject InstantiateFish(Vector2 position) {
        GameObject newFish = Instantiate(FishPrefabs[Random.Range(0, FishPrefabs.Count)], new Vector3(position.x, position.y, 0), Quaternion.identity, this.transform);
        Fishes.Add(newFish);
        return newFish;
    }

    public GameObject InstantiateFish(Vector2 position, int fish) {
        GameObject newFish = Instantiate(FishPrefabs[fish], new Vector3(position.x, position.y, 0), Quaternion.identity, this.transform);
        Fishes.Add(newFish);
        newFish.GetComponent<FishController>().enabled = false;
        newFish.GetComponent<FishBrain>().enabled = false;
        newFish.GetComponentInChildren<Bobbing>().enabled = false;
        newFish.GetComponentInChildren<SpriteAnimation>().enabled = false;
        newFish.GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

        return newFish;
    }

    public GameObject GetFish(int index) {
        return (Fishes[index]);
    }

    public int GetFishIndex(GameObject fish) {
        for (int i = 0; i < Fishes.Count; i++)
        {
            if (fish == Fishes[i])
            {
                return (i);
            }
        }
        return (-1);
    }

    public GameObject ClosestFish(Vector2 pos) {
        GameObject closest = Fishes[0];
        Vector2 closestPos = new Vector2(closest.transform.position.x, closest.transform.position.y);
        Vector2 thisPos;
        foreach (GameObject fish in Fishes)
        {
            if (!fish.GetComponent<FishBrain>().dead)
            {
                thisPos = new Vector2(fish.transform.position.x, fish.transform.position.y);
                if (thisPos.magnitude < closestPos.magnitude && thisPos != pos)
                {
                    closestPos = thisPos;
                    closest = fish;
                }
            }
        }
        return (closest);
    }

    public List<GameObject> FishCloserThan(Vector2 pos, float dist) {

        List<GameObject> closerFishes = new List<GameObject>();
        Vector3 fishPos = Vector3.zero;
        foreach (GameObject fish in Fishes)
        {
            if (fish == null)
            {
                //break;
            }
            if (!fish.GetComponent<FishBrain>().dead)
            {
                fishPos = fish.transform.position;
                //If inside circle centered at pos of radius dist
                if (fishPos.x > pos.x - dist && fishPos.x < pos.x + dist && fishPos.y > pos.y - dist && fishPos.y < pos.y + dist)
                {
                    closerFishes.Add(fish);
                }
            }
        }
        return (closerFishes);
    }

    public GameObject FurthestFish(Vector2 pos) {
        GameObject furthest = Fishes[0];
        Vector2 closestPos = new Vector2(furthest.transform.position.x, furthest.transform.position.y);
        Vector2 thisPos;
        foreach (GameObject fish in Fishes)
        {
            if (!fish.GetComponent<FishBrain>().dead)
            {
                thisPos = new Vector2(fish.transform.position.x, fish.transform.position.y);
                if (thisPos.magnitude > closestPos.magnitude && thisPos != pos)
                {
                    closestPos = thisPos;
                    furthest = fish;
                }
            }
        }
        return (furthest);
    }

    public void DeleteFish(GameObject fishToGo) {
        print("Deleting");
        List<GameObject> newFishes = new List<GameObject>();
        /*foreach (GameObject fish in Fishes)
        {
            if (fish != fishToGo)
            {
                newFishes.Add(fish);
            }
        }*/
        foreach (Transform t in this.transform)
        {
            if (t.name == "BasicFish(Clone)" || t.name == "ThinFish(Clone)" || t.name == "CatFish(Clone)")
            {
                if (t.gameObject != fishToGo)
                {
                    newFishes.Add(t.gameObject);
                }
            }
        }
        Fishes = newFishes;
    }

    public GameObject RandomFish(GameObject self) {
        GameObject choice = self;
        while (choice != self)
        {
            choice = Fishes[Random.Range(0, Fishes.Count)];
        }
        return (choice);
    }
}
