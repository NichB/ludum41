﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour {

    public GameObject battleButton;
    public int fishThreshold = 12;

    public void fightButtonPress() {
        battleButton.SetActive(false);
    }

    private FishMastermind control;

    private void Start() {
        control = this.GetComponent<FishMastermind>();
    }

    private void FixedUpdate() {
        if (control.getFishCount() > fishThreshold)
        {
            battleButton.SetActive(true);
        }
    }
}
