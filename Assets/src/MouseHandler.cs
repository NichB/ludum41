﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseHandler : MonoBehaviour {

    public int state = 0;
    public int placing = 0;

    public GameObject UI;

    public List<GameObject> prefabs = new List<GameObject>();


    public GameObject tank;
    public EventSystem eventSys;


    public AudioClip[] splashes;
    public AudioClip[] clicks;

    public AudioClip musicBattle;

    private AudioSource audioSource;

    private FishMastermind fishControl;
    private PlantMastermind plantControl;

    private GameObject ghost = null;
    private bool placed = false;

    Vector3 worldPos;

    // Use this for initialization
    void Start () {
        fishControl = tank.GetComponent<FishMastermind>();
        plantControl = tank.GetComponent<PlantMastermind>();
        audioSource = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        HandleMouseInput();
	}

    public void battleMusic() {
        audioSource.Stop();
        audioSource.clip = musicBattle;
        audioSource.loop = true;
        audioSource.Play();
    }

    public void disableUI() {
        UI.SetActive(false);
    }

    private void playSplash() {
        AudioClip sound = splashes[Random.Range(0, splashes.Length)];
        audioSource.PlayOneShot(sound, 1f);
    }

    private void playClick() {
        AudioClip sound = clicks[Random.Range(0, clicks.Length)];
        audioSource.PlayOneShot(sound, 1f);
    }

    private void HandleMouseInput() {
        Vector3 mousePos = Input.mousePosition;
        worldPos = Camera.main.ScreenToWorldPoint(mousePos);

        if (Input.GetMouseButtonDown(0) && !eventSys.IsPointerOverGameObject()) {

            switch (state) {
                case 0:
                    List<GameObject> closeFishes = new List<GameObject>();
                    Vector2 worldPos2d = new Vector2(worldPos.x, worldPos.y);
                    closeFishes = fishControl.FishCloserThan(worldPos2d, 10f);
                    foreach (GameObject fish in closeFishes) {
                        fish.GetComponent<FishBrain>().FleeFrom(worldPos);
                    }
                    break;
                case 1:
                    switch (placing) {
                        case 0:
                            placeBasicFish();
                            break;
                        case 1:
                            placeThinFish();
                            break;
                        case 2:
                            placePlant();
                            break;
                        case 3:
                            placeReed();
                            break;
                        case 4:
                            placeDebris();
                            break;
                        case 5:
                            placeCatFish();
                            break;
                        default:
                            Debug.Log("Incorrect placement Attempted");
                            break;
                    }
                    state = 0;
                    placed = true;
                    break;
            }
        }
        
        if (state == 1) {
            placed = false;
            if (placing >= 2 && placing <= 4) {
                ghost.transform.position = new Vector3(worldPos.x,-18.5f, 0f);
            } else {
                ghost.transform.position = new Vector3(worldPos.x, Mathf.Clamp(worldPos.y,-18f,19f),0f);
            }
        }
    }

    public void selectFishB() {
        state = 1;
        placing = 0;
        if (!placed && ghost != null) {
            fishControl.DeleteFish(ghost);
            Destroy(ghost);
            ghost = null;
        }
        ghost = fishControl.InstantiateFish(worldPos, 0);
        playClick();
    }
    public void selectFishT() {
        state = 1;
        placing = 1;
        if (!placed && ghost != null) {
            fishControl.DeleteFish(ghost);
            Destroy(ghost);
            ghost = null;
        }
        ghost = fishControl.InstantiateFish(worldPos, 1);
        playClick();
    }
    public void selectFishC() {
        state = 1;
        placing = 5;
        if (!placed && ghost != null) {
            fishControl.DeleteFish(ghost);
            Destroy(ghost);
            ghost = null;
        }
        ghost = fishControl.InstantiateFish(worldPos, 2);
        playClick();
    }
    public void selectPlant() {
        state = 1;
        placing = 2;
        if (!placed && ghost != null) {
            plantControl.DeletePlant(ghost);
            Destroy(ghost);
            ghost = null;
        }
        ghost = plantControl.InstantiatePlant(worldPos, 1);
        playClick();
    }
    public void selectReed() {
        state = 1;
        placing = 3;
        if (!placed && ghost != null) {
            plantControl.DeletePlant(ghost);
            Destroy(ghost);
            ghost = null;
        }
        ghost = plantControl.InstantiatePlant(worldPos, 0);
        playClick();
    }
    public void selectDebris() {
        state = 1;
        placing = 4;
        if (!placed && ghost != null) {
            plantControl.DeleteDebris(ghost);
            Destroy(ghost);
            ghost = null;
        }
        ghost = plantControl.InstantiateDebris(worldPos);
        playClick();
    }

    private void placeBasicFish() {
        ghost.GetComponent<FishController>().enabled = true;
        ghost.GetComponent<FishBrain>().enabled = true;
        //ghost.GetComponentInChildren<Bobbing>().enabled = true;
        ghost.GetComponentInChildren<SpriteAnimation>().enabled = true;
        ghost.GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1);
        playSplash();
    }

    private void placeThinFish() {
        ghost.GetComponent<FishController>().enabled = true;
        ghost.GetComponent<FishBrain>().enabled = true;
        //ghost.GetComponentInChildren<Bobbing>().enabled = true;
        ghost.GetComponentInChildren<SpriteAnimation>().enabled = true;
        ghost.GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1);
        playSplash();
    }

    private void placeCatFish() {
        ghost.GetComponent<FishController>().enabled = true;
        ghost.GetComponent<FishBrain>().enabled = true;
        //ghost.GetComponentInChildren<Bobbing>().enabled = true;
        ghost.GetComponentInChildren<SpriteAnimation>().enabled = true;
        ghost.GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1);
        playSplash();
    }

    private void placePlant() {
        ghost.GetComponent<RandomSpriteChooser>().solidify();
        playSplash();
    }

    private void placeReed() {
        ghost.GetComponent<RandomSpriteChooser>().solidify();
        playSplash();
    }

    private void placeDebris() {
        ghost.GetComponent<RandomSpriteChooser>().solidify();
        playSplash();
    }
}
