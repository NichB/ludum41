﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputHandler : MonoBehaviour {

    private FishMastermind fishControl;
    private PlantMastermind plantControl;

    private void Start() {
        fishControl = GetComponent<FishMastermind>();
        plantControl = GetComponent<PlantMastermind>();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    public void placeBasicFish() {
        Vector2 newPos = new Vector2(Random.Range(-33f, 33f), Random.Range(-18f, 18f));

        fishControl.InstantiateFish(newPos, 0);
    }

    public void placeThinFish() {
        Vector2 newPos = new Vector2(Random.Range(-33f, 33f), Random.Range(-18f, 18f));

        fishControl.InstantiateFish(newPos, 1);
    }

    public void placePlant() {
        Vector2 newPos = new Vector2(Random.Range(-33f, 33f), -18.5f);

        plantControl.InstantiatePlant(newPos, 1);
    }

    public void placeReed() {
        Vector2 newPos = new Vector2(Random.Range(-33f, 33f), -18.5f);

        plantControl.InstantiatePlant(newPos, 0);
    }

    public void placeDebris() {
        Vector2 newPos = new Vector2(Random.Range(-33f, 33f), -18.5f);

        plantControl.InstantiateDebris(newPos);
    }
}
