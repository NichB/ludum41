﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobbing : MonoBehaviour {

    public float bobbingDistance = 1f;

    public float speed = 1f;
    bool movingUp = true;
    float velocity = 0f;

    //private float maxSpeed = 10.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //float factor = Mathf.Abs(transform.position.y) / bobbingDistance;
        //factor = factor > 0.7f ? 0.5f + 0.5f*(((1.0f/(factor*2))/2) + 0.1f) : 1.0f;
        //factor = 1f;
        //transform.Translate(Vector3.up * velocity * factor);
        transform.Translate(new Vector3(0f, velocity, 0f));

        if (this.transform.position.y > bobbingDistance)
        {
            movingUp = false;
        }
        if (this.transform.position.y < -bobbingDistance)
        {
            movingUp = true;
        }

        if (movingUp)
        {
            velocity = speed;

        } else {
            velocity = -speed;
        }
    }
}
