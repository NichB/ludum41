﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float speed = 3f;

	// Use this for initialization
	void Start () {
		 
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 camPos = transform.position;
		if (Input.mousePosition.x > Screen.width*0.9f) {
             camPos.x += speed * Time.deltaTime;
        }

        transform.position = camPos;
	}
}
