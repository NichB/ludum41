﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpriteChooser : MonoBehaviour {

    public Sprite[] spriteOptions;

    private SpriteRenderer rend;
    private float variator;

    void Start () {
        rend = GetComponent<SpriteRenderer>();

        int randomIndex = Random.Range(0, spriteOptions.Length);

        rend.sprite = spriteOptions[randomIndex];
        
        rend.flipX = (Random.value > 0.5f);

        variator = 0.85f + (Random.value * 0.3f);

        if (rend.color.maxColorComponent == rend.color.r) {
            rend.color = new Color(rend.color.r * variator, rend.color.g, rend.color.b, 0.5f);
        } else if (rend.color.maxColorComponent == rend.color.g) {
            rend.color = new Color(rend.color.r, rend.color.g * variator, rend.color.b, 0.5f);
        } else {
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b * variator, 0.5f);
        }

        transform.localScale *= 0.8f + (Random.value * 0.4f);

        if (gameObject.tag == "Ground") {
            solidify();
        }
    }

    public void solidify() {
        if (rend.color.maxColorComponent == rend.color.r) {
            rend.color = new Color(rend.color.r * variator, rend.color.g, rend.color.b);
        } else if (rend.color.maxColorComponent == rend.color.g) {
            rend.color = new Color(rend.color.r, rend.color.g * variator, rend.color.b);
        } else {
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b * variator);
        }
    }


}
