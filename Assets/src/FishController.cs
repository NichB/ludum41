﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishController : MonoBehaviour {

    public float speed = 1.0f;
    public float drag = 1.0f;

    public float attackAcceleration = 5.0f;

    private bool attacking = false;
    private float timeoutAt = 0f;

    private Vector2 velocity;
    private Vector2 acceleration;
    private Transform thisFishyTransform;

    private GameObject child;
    private Bobbing childsBobbingScript;
    private SpriteRenderer childRenderer;

    public float bobbingSpeed = 0.001f;
    public float bobbingDistance = 0.2f;

	// Use this for initialization
	void Start () {
        velocity = Vector2.zero;
        acceleration = Vector2.zero;
        thisFishyTransform = this.gameObject.transform;

        child = this.gameObject.transform.GetChild(0).gameObject;
        childsBobbingScript = child.GetComponent<Bobbing>();
        childRenderer = child.GetComponent<SpriteRenderer>();
        childsBobbingScript.speed = bobbingSpeed;
        childsBobbingScript.bobbingDistance = bobbingDistance;
        /*
        float randomHue;

        do
        {
            randomHue = Random.Range(0f, 1f);
        } while (randomHue < 0.7f && randomHue > 0.4f);
        */
        childRenderer.color = Random.ColorHSV(0.8f, 0.9f, 0.02f, 0.1f, 0.9f, 1f);
        
    }    

    public void Move(Vector2 Direction) {
        acceleration = Direction;
    }

    private void FixedUpdate() {
        //HandleInput();
        
        HandleAcceleration();
        HandleVelocity();
        HandleBobbing();


    }

    public bool Attack(Vector2 direction) {
        if (!attacking)
        {
            direction.Normalize(); //Assure unit vector

            attacking = true;
            timeoutAt = Time.time + 2f;
            acceleration = attackAcceleration * direction;

            return (true); //successful
        }
        return (false); //unsuccessful
    }

    public void Knockback(Vector2 Direction) {
        acceleration = Direction;
    }

    public void Die() {
        childRenderer.flipY = true; //RIP
    }

    private void HandleBobbing() {
        if (acceleration.magnitude != 0)
        {
            childsBobbingScript.speed = 0.0f;
        } else
        {
            childsBobbingScript.speed = bobbingSpeed;
        }
    }

    private void HandleInput() {
        int accelY = 0;
        accelY += Input.GetKey(KeyCode.W) ? 1 : 0;
        accelY -= Input.GetKey(KeyCode.S) ? 1 : 0;
        acceleration.y = (float)Mathf.Clamp(accelY, -1, 1);
    }

    private void HandleVelocity() {
        thisFishyTransform.Translate(new Vector3(velocity.x, velocity.y, 0.0f));
        velocity = velocity / drag;
    }

    private void HandleAcceleration() {
        Vector2 normalisedAccel = Vector2.zero;
        if (!attacking)
        {
            normalisedAccel = new Vector2(acceleration.x, acceleration.y);
            normalisedAccel.Normalize();
            velocity = velocity + (normalisedAccel * (speed / 100));
        }
        else
        {
            normalisedAccel = new Vector2(acceleration.x, acceleration.y);
            velocity = velocity + (normalisedAccel * (speed / 100));
            if (Time.time > timeoutAt)
            {
                attacking = false;
            }
        }
        
        if (normalisedAccel.x < 0)
        {
            if (childRenderer.flipX)
            {
                childRenderer.flipX = false;
            }
        }
        else if (normalisedAccel.x > 0)
        {
            if (!childRenderer.flipX)
            {
                childRenderer.flipX = true;
            }
        }
    }

}
