﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGenerator : MonoBehaviour {

    public GameObject GroundPrefab;
    public int noGround;
    public float minSpacing;
    public float maxSpacing;

    // Use this for initialization
    void Start () {

        Vector2 newPos = new Vector2(-35f,-18.5f);
    
        for (int i = 0; i < noGround; i++) {
            
            GameObject newFish = Instantiate<GameObject>(GroundPrefab, new Vector3(newPos.x, newPos.y, 0), Quaternion.identity, this.transform);
            newPos.x += Random.Range(minSpacing, maxSpacing);
        }
    }
	
}
