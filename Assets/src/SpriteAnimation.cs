﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour {

    public Sprite[] frames;
    public float secondsPerFrame = 0.5f;

    private int frameIndex = 0;
    private float timer = 0f;
    private SpriteRenderer rend;

	// Use this for initialization
	void Start () {
        rend = GetComponent<SpriteRenderer>();
        rend.sprite = frames[0];
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer > secondsPerFrame) {
            timer -= secondsPerFrame;
            if (frameIndex < (frames.Length - 1)) {
                frameIndex++;
            } else { frameIndex = 0; }
            rend.sprite = frames[frameIndex];
        }
	}
}
