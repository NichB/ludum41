﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantMastermind : MonoBehaviour {

    public int noPlants = 8;

    public List<GameObject> PlantPrefabs;

    public int noDebris = 5;

    public List<GameObject> DebrisPrefabs;

    public float zLevel = 1f;

    private List<GameObject> Plants = new List<GameObject>();

    private List<GameObject> Debris = new List<GameObject>();

    // Use this for initialization
    void Start () {
        //Instantiate plants
        Vector2 newPos = Vector2.zero;
        for (int i = 0; i < noPlants; i++)
        {
            newPos = new Vector2(Random.Range(-33f, 33f), -18.5f);
            InstantiatePlant(newPos);
        }

        //Instantiate debris
        newPos = Vector2.zero;
        for (int i = 0; i < noPlants; i++)
        {
            newPos = new Vector2(Random.Range(-33f, 33f), -18.5f);
            InstantiateDebris(newPos);
        }
    }
    
    public GameObject InstantiatePlant(Vector2 position) {
        GameObject newPlant = Instantiate<GameObject>(PlantPrefabs[Random.Range(0, PlantPrefabs.Count)], new Vector3(position.x, position.y, zLevel), Quaternion.identity, this.transform);
        Plants.Add(newPlant);
        return newPlant;
    }

    public GameObject InstantiatePlant(Vector2 position, int plant) {
        GameObject newPlant = Instantiate<GameObject>(PlantPrefabs[plant], new Vector3(position.x, -18.5f, zLevel), Quaternion.identity, this.transform);

        newPlant.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

        Plants.Add(newPlant);
        return newPlant;
    }

    public GameObject InstantiateDebris(Vector2 position) {
        GameObject newDebris = Instantiate<GameObject>(DebrisPrefabs[Random.Range(0, DebrisPrefabs.Count)], new Vector3(position.x, -18.5f, Random.Range(zLevel - 0.5f, zLevel + 0.5f)), Quaternion.identity, this.transform);
        Debris.Add(newDebris);

        newDebris.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

        return newDebris;
    }

    public void DeletePlant(GameObject fishToGo) {
        List<GameObject> newFishes = new List<GameObject>();
        foreach (GameObject fish in Plants)
        {
            if (fish != fishToGo)
            {
                newFishes.Add(fish);
            }
        }
        Plants = null;
        Plants = newFishes;
    }

    public void DeleteDebris(GameObject fishToGo) {
        List<GameObject> newFishes = new List<GameObject>();
        foreach (GameObject fish in Debris)
        {
            if (fish != fishToGo)
            {
                newFishes.Add(fish);
            }
        }
        Debris = null;
        Debris = newFishes;
    }
}
