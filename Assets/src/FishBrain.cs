﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishBrain : MonoBehaviour {
    //1 unit = 1 centimetre

    public Vector2 Position;

    public float minTimeTillChange = 5f;
    public float maxTimeTillChange = 30f;

    public bool debugLine = false;

    public float fleeDither = 0.2f;
    public float skitishness = 5f;

    public bool attacking = false;
    public bool dead = false;

    public float health = 100f;

    public bool battleRoyale = false;

    private LineRenderer line;
    private FishController control;
    private SpriteRenderer thisSprite;
    private float newTargetIn = 0.0f;
    private Vector2 destination;
    private bool hasTarget = false;

    private GameObject targetFish;
    private FishBrain targetFishBrain;



	// Use this for initialization
	void Start () {
        control = this.gameObject.GetComponent<FishController>();
        destination = new Vector2(this.transform.position.x, this.transform.position.y);
        line = this.GetComponent<LineRenderer>();
        thisSprite = this.gameObject.GetComponentInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Color temp = thisSprite.color;
        temp.a = 1f;
        thisSprite.color = temp;
        HandlePathing();
        if (battleRoyale && attacking && !dead && hasTarget)
        {
            HandleAttacking();
        }
        HandleHealth();
    }

    public void ChangeDirection() {
        //New destination and timer
        destination = ChooseTarget();
        newTargetIn = Random.Range(Time.unscaledTime, Time.unscaledTime + Random.Range(minTimeTillChange, maxTimeTillChange));
    }

    public void FleeFrom(Vector2 pos) {
        //First, get the vector from the position to the fish
        Vector2 incidence = new Vector2(this.transform.position.x - pos.x, this.transform.position.y - pos.y);

        float distance = incidence.magnitude + 0.0001f; //Make sure != 0

        //Secondly get the angle from (1,0)
        incidence.Normalize();
        float theta = Mathf.Atan2(incidence.y, incidence.x);

        //Then vary our angle a little bit
        theta = Random.Range(theta - fleeDither, theta + fleeDither);

        //And construct our new vector from this angle
        Vector2 direction = new Vector2(Mathf.Cos(theta), Mathf.Sin(theta));

        direction = direction * Random.Range(Mathf.Clamp(1f/distance*5f, 1f, 10f), Mathf.Clamp(1f/distance*skitishness*5f, 1f, 10f));

        //Apply to the fish
        destination = direction + new Vector2(this.transform.position.x, this.transform.position.y);
    }

    public void KnockbackFrom(Vector2 pos) {
        //First, get the vector from the position to the fish
        Vector2 incidence = new Vector2(this.transform.position.x - pos.x, this.transform.position.y - pos.y);

        float mag = incidence.magnitude + 0.0001f; //Make sure != 0

        //Secondly get the angle from (1,0)
        incidence.Normalize();
        float theta = Mathf.Atan2(incidence.y, incidence.x);

        //Then vary our angle a little bit
        theta = Random.Range(theta - fleeDither, theta + fleeDither);

        //And construct our new vector from this angle
        Vector2 direction = new Vector2(Mathf.Cos(theta), Mathf.Sin(theta));

        direction = direction * Random.Range(Mathf.Clamp(1f / mag * 5f, 5f, 15f), Mathf.Clamp(1f / mag * skitishness * 5f, 5f, 15f));

        //Apply to the fish
        control.Knockback( new Vector2(this.transform.position.x, this.transform.position.y) );
    }

    private void AttackPositions(Vector2 pos) {
        //attacking = true;

        //First, dither the position given by +- 3f
        Vector2 ditheredPosition = new Vector2(Random.Range(pos.x - 3f, pos.x + 3f), Random.Range(pos.y - 3f, pos.y + 3f));

        //Then set the destination to be the position
        //Vector2 incidence = new Vector2(this.transform.position.x - ditheredPosition.x, this.transform.position.y - ditheredPosition.y);
        destination = ditheredPosition;
    }
    
    public void Attack(GameObject target) {
        attacking = true;
        hasTarget = true;
        targetFish = target;
        targetFishBrain = target.GetComponent<FishBrain>();
    }

    public void Die() {
        control.Die();
        destination = new Vector2(this.transform.position.x, -18.5f);
        dead = true;
        attacking = false;
        hasTarget = false;
        this.GetComponentInChildren<SpriteAnimation>().enabled = false;
    }

    public void hurt(float damage, Vector2 posFrom, GameObject attacker) {
        health -= damage;
        KnockbackFrom(posFrom);
        //Self defence
        if (attacking && hasTarget)
        {
            float test = Random.Range(0f, 2f);
            if (test >= 1f)
            {
                Attack(attacker);
            }
        }
        else
        {
            Attack(attacker);
        }
        Color temp = thisSprite.color;
        temp.a = 0.5f;
        thisSprite.color = temp;
    }

    private void HandleAttacking() {

        if (targetFishBrain.dead)
        {
            print("Victory!");
            attacking = false;
            targetFish = null;
            hasTarget = false;
        }
        else
        {
            if (new Vector2(targetFish.transform.position.x - this.transform.position.x, targetFish.transform.position.y - this.transform.position.y).magnitude > 2f)
            {
                AttackPositions(new Vector2(targetFish.transform.position.x, targetFish.transform.position.y));
            }
            else
            {
                if (control.Attack(new Vector2(targetFish.transform.position.x, targetFish.transform.position.y)))
                {
                    targetFishBrain.hurt(34f, new Vector2(this.transform.position.x, this.transform.position.y), this.gameObject);
                }
            }
        }
    }

    private void HandleHealth() {
        if (health <= 0f)
        {
            Die();
        }
    }

    private void HandlePathing() {
        if (newTargetIn < Time.unscaledTime && !attacking && !dead)
        {
            ChangeDirection();
        }

        if (dead)
        {
            destination = new Vector2(this.transform.position.x, -18.5f);
        }

        control.Move(ChoosePathFromHere());
        if (debugLine)
        {
            line.SetPosition(0, this.transform.position);
            line.SetPosition(1, new Vector3(destination.x, destination.y, 0));
        }
        
    }

    private Vector2 ChoosePathFromHere() {
        if (new Vector2(destination.x - this.transform.position.x, destination.y - this.transform.position.y).magnitude > 0.4f) 
        {
            return (new Vector2(destination.x - this.transform.position.x, destination.y - this.transform.position.y));
        }
        else
        {
            return (Vector2.zero);
        }
    }

    private Vector2 ChooseTarget() {
        float x = Random.Range(-33f, 33f);
        float y = Random.Range(-18f, 18f);
        return (new Vector2(x, y));
    }
}
